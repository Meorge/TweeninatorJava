package Tweeninator;

import java.util.ArrayList;

public class TweenGroup
{
    private ArrayList<Tween> tweens = new ArrayList<>();

    public TweenGroup() {}

    public boolean getCompleted()
    {
        for (Tween t : tweens)
        {
            if (t.getState() != TweenState.Completed) return false;
        }
        return true;
    }

    public void update(final int delta)
    {
        for (Tween t : tweens) t.update(delta);
    }

    public TweenGroup addTween(final Tween tween)
    {
        tweens.add(tween);
        return this;
    }

    public TweenGroup reset()
    {
        for (Tween t : tweens) t.reset();
        return this;
    }
}

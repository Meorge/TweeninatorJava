package Tweeninator;

import java.util.ArrayList;

public class TweenManager
{
    private static final ArrayList<TweenSequence> sequences = new ArrayList<>();

    public static void update(final int delta)
    {
        for (TweenSequence s : new ArrayList<TweenSequence>(sequences)) s.update(delta);
    }

    public static void add(final TweenSequence sequence)
    {
        sequences.add(sequence);
    }

    public static void remove(final TweenSequence sequence) { sequences.remove(sequence); }
}

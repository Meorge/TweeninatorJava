package Tweeninator;

import java.util.function.Function;

// Easing functions from https://easings.net
public class TweenEasings
{
    public static Function<Float, Float> InSine()
    {
        return (t) -> { return (float)(1.0f - Math.cos((t * Math.PI) / 2.0f)); };
    }

    public static Function<Float, Float> OutSine()
    {
        return (t) -> { return (float)(Math.sin((t * Math.PI) / 2.0f)); };
    }

    public static Function<Float, Float> InOutSine()
    {
        return (t) -> { return (float)(-(Math.cos(t * Math.PI) - 1) / 2.0f); };
    }

    public static Function<Float, Float> InCubic()
    {
        return (t) -> { return t * t * t; };
    }

    public static Function<Float, Float> OutCubic()
    {
        return (t) -> { return 1.0f - (float)Math.pow(1 - t, 3); };
    }

    public static Function<Float, Float> InOutCubic()
    {
        return (t) -> {
            return t < 0.5f ?
                    4 * t * t * t :
                    1 - (float)Math.pow(-2 * t + 2, 3) / 2
                    ;
        };
    }

    public static Function<Float, Float> InQuint()
    {
        return (t) -> { return t * t * t * t * t; };
    }

    public static Function<Float, Float> OutQuint()
    {
        return (t) -> { return 1 - (float)Math.pow(1 - t, 5); };
    }

    public static Function<Float, Float> InOutQuint()
    {
        return (t) -> {
            return t < 0.5f ?
                    16 * (float)Math.pow(t, 5) :
                    1 - (float)Math.pow(-2 * t + 2, 5) / 2;
        };
    }

    public static Function<Float, Float> InCirc()
    {
        return (t) -> { return 1 - (float)Math.sqrt(1 - Math.pow(t, 2)); };
    }

    public static Function<Float, Float> OutCirc()
    {
        return (t) -> { return (float)Math.sqrt(1 - Math.pow(t - 1, 2)); };
    }

    public static Function<Float, Float> InOutCirc()
    {
        return (t) -> {
            return t < 0.5f ?
                (1 - (float)Math.sqrt(1 - Math.pow(2 * t, 2))) / 2 :
                (float)(Math.sqrt(1 - Math.pow(-2 * t + 2, 2)) + 1) / 2;
        };
    }

    public static Function<Float, Float> InQuad()
    {
        return (t) -> { return t * t; };
    }

    public static Function<Float, Float> OutQuad()
    {
        return (t) -> { return 1 - (1 - t) * (1 - t); };
    }

    public static Function<Float, Float> InOutQuad()
    {
        return (t) -> {
            return t < 0.5f ?
                    2 * t * t :
                    1 - (float)Math.pow(-2 * t + 2, 2) / 2;
        };
    }

    public static Function<Float, Float> InQuart()
    {
        return (t) -> { return t * t * t * t; };
    }

    public static Function<Float, Float> OutQuart()
    {
        return (t) -> { return 1 - (float)Math.pow(1 - t, 4); };
    }

    public static Function<Float, Float> InOutQuart()
    {
        return (t) -> {
            return t < 0.5f ?
                    8 * t * t * t * t :
                    1 - (float)Math.pow(-2 * t + 2, 4) / 2;
        };
    }

    public static Function<Float, Float> InExpo()
    {
        return (t) -> {
            return t == 0 ?
                    0 :
                    (float)Math.pow(2, 10 * t - 10);
        };
    }

    public static Function<Float, Float> OutExpo()
    {
        return (t) -> {
            return t == 1 ?
                    1 :
                    1 - (float)Math.pow(2, -10 * t);
        };
    }

    public static Function<Float, Float> InOutExpo()
    {
        return (t) -> {
            return t == 0 ?
                    0 :
                    t == 1 ?
                        1 :
                        t < 0.5f ?
                                (float)Math.pow(2, 20 * t - 10) / 2.0f :
                                (2 - (float)Math.pow(2, -20 * t + 10)) / 2;
        };
    }

    public static Function<Float, Float> InBack() { return InBack(1.7f); }
    public static Function<Float, Float> InBack(float overshoot)
    {
        return (t) -> {
            float c1 = overshoot;
            float c3 = c1 + 1;
            return c3 * t * t * t - c1 * t * t;
        };
    }

    public static Function<Float, Float> OutBack() { return OutBack(1.7f); }
    public static Function<Float, Float> OutBack(float overshoot)
    {
        return (t) -> {
            // EaseOutBack code from https://easings.net/#easeOutBack
            float c1 = overshoot;
            float c3 = c1 + 1;
            return 1 + c3 * (float)Math.pow(t - 1, 3) + c1 * (float)Math.pow(t - 1, 2);
        };
    }

    public static Function<Float, Float> InOutBack() { return InOutBack(1.7f); }
    public static Function<Float, Float> InOutBack(float overshoot)
    {
        return (t) -> {
            float c1 = overshoot;
            float c2 = c1 * 1.525f;
            return t < 0.5f ?
                    ((float)Math.pow(2 * t, 2) * ((c2 + 1) * 2 * t - c2)) / 2 :
                    ((float)Math.pow(2 * t - 2, 2) * ((c2 + 1) * (t * 2 - 2) + c2) + 2) / 2;
        };
    }
}

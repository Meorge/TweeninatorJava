package Tweeninator;

public enum TweenState {
    Created,
    Playing,
    Completed
}

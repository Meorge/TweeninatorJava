package Tweeninator;

import java.util.ArrayList;

public class TweenSequence
{
    private ArrayList<TweenGroup> groups = new ArrayList<>();
    private boolean repeat = false;

    public boolean isComplete()
    {
        for (TweenGroup g : groups)
        {
            if (!g.getCompleted()) return false;
        }
        return true;
    }

    public TweenSequence setRepeat(boolean repeat)
    {
        this.repeat = repeat;
        return this;
    }

    public TweenSequence then(Tween tween)
    {
        TweenGroup newGroup = new TweenGroup();
        newGroup.addTween(tween);
        groups.add(newGroup);
        return this;
    }

    public TweenSequence and(Tween tween)
    {
        groups.get(groups.size() - 1).addTween(tween);
        return this;
    }

    public void update(final int delta)
    {
        TweenGroup groupToEval = getFirstUnfinishedTweenGroup();
        if (groupToEval != null) groupToEval.update(delta);
        else
        {
            if (!repeat)
            {
                return;
            }
            else
                for (TweenGroup g : groups) g.reset();
        }
    }

    private TweenGroup getFirstUnfinishedTweenGroup()
    {
        for (TweenGroup g : groups)
        {
            if (!g.getCompleted()) return g;
        }
        return null;
    }
}

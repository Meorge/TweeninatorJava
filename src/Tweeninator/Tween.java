package Tweeninator;

import java.util.function.DoubleConsumer;
import java.util.function.DoubleSupplier;
import java.lang.Runnable;
import java.util.function.Function;

public class Tween {
    private float startValue;
    private float endValue;

    private int timeSoFar = 0;
    private int duration = 0;

    private float percentComplete = 0.0f;

    private final DoubleSupplier getter;
    private final DoubleConsumer setter;

    private Function<Float, Float> easeFunction = (v) -> { return v; };

    private Runnable onComplete = null;

    private TweenState state = TweenState.Created;




    public Tween(DoubleSupplier getter, DoubleConsumer setter)
    {
        this.getter = getter;
        this.setter = setter;
    }

    public Tween()
    {
        this.getter = () -> 0f;
        this.setter = (x) -> {};
    }

    public static Tween wait(final int ms) { return new Tween().withDuration(ms); }
    public static Tween run(final Runnable action) { return new Tween().runOnComplete(action); }

    public Tween to(float endValue)
    {
        this.endValue = endValue;
        return this;
    }

    public Tween withDuration(final int ms)
    {
        this.duration = ms;
        return this;
    }

    public Tween withEase(final Function<Float, Float> easeFunction)
    {
        this.easeFunction = easeFunction;
        return this;
    }

    public Tween runOnComplete(final Runnable onComplete)
    {
        this.onComplete = onComplete;
        return this;
    }

    public Tween reset()
    {
        percentComplete = 0.0f;
        timeSoFar = 0;
        state = TweenState.Playing;
        setter.accept(startValue);
        return this;
    }

    public TweenState getState() { return state; }

    public void update(final int delta)
    {
        // Initialize tween if this is the first frame it's run
        if (state == TweenState.Created)
        {
            startValue = (float)getter.getAsDouble();
            state = TweenState.Playing;
        }

        if (state == TweenState.Playing)
        {
            // Determine how far in the transition from A to B we are
            timeSoFar += delta;
            percentComplete = duration > 0 ? (float)timeSoFar / (float)duration : 1f;

            // Lerp the percent complete to the target value
            // (formula from https://www.gamedev.net/tutorials/programming/general-and-gameplay-programming/inverse-lerp-a-super-useful-yet-often-overlooked-function-r5230/)
            float tempPercentComplete = this.easeFunction.apply(percentComplete);
            float newValue = (float)((1.0 - tempPercentComplete) * startValue + endValue * tempPercentComplete);

            // Set the new value
            setter.accept(newValue);

            if (percentComplete >= 1.0f)
            {
                // When the tween is complete, set it to the end value one last time (in case of overshoot),
                // then set its state to Completed
                percentComplete = 1.0f;
                setter.accept(endValue);

                // Run the onComplete function, if it was defined
                if (onComplete != null)
                {
                    onComplete.run();
                }
                state = TweenState.Completed;
            }
        }
    }
}
